package com.laoxu.springboot.config;

import com.laoxu.springboot.service.HelloService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * bean配置类
 *
 * @author xusucheng
 * @create 2019-01-07
 **/
@Configuration
public class MyAppConfig {
    @Bean
    public HelloService helloService(){
        return new HelloService();
    }
}
