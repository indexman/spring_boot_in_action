package com.laoxu.springboot.bean;

import com.laoxu.springboot.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试Bean加载
 *
 * @author xusucheng
 * @create 2019-01-07
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class BeanTest {
    @Autowired
    ApplicationContext ioc;

    @Test
    public void testHelloService(){
        System.out.println("==============");
        System.out.println(ioc.containsBean("helloService"));
    }
}
