package com.laoxu.springboot.bean;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xusucheng
 * @create 2019-01-06
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonTest {
    @Autowired
    Person person;

    @Test
    public void testPerson(){
        System.out.println(person);
    }
}
