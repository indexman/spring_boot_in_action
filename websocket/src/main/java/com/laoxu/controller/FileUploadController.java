package com.laoxu.controller;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author laoxu
 * @Date 2021-11-12 11:01
 **/
@Controller
@RequestMapping("/api/file")
public class FileUploadController {
    @RequestMapping(value="/upload", method=RequestMethod.POST )
    @ResponseBody
    public Map<String,Object> uploadFile(@RequestParam("fileItem") MultipartFile fileItem )
    {

        String filename = fileItem.getOriginalFilename();
        try {
            byte[] fileBytes = fileItem.getBytes();
            Files.write(Paths.get("D:\\tmp\\" + fileItem.getOriginalFilename()), fileBytes);
        } catch (IOException e) {
            e.printStackTrace();

        }
        Map<String,Object> result = new HashMap<>();
        result.put("code",200);
        result.put("message",filename+"上传成功！");

        return result;
    }
}
