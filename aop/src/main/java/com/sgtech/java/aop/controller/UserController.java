package com.sgtech.java.aop.controller;

import com.sgtech.java.aop.annotation.OperLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 模拟用户增删改查接口
 * @Author laoxu
 * @Date 2021-10-19 15:18
 **/
@RestController
@RequestMapping("/user")
public class UserController {
    @OperLog(operModule = "系统管理",operType = "新增",operDesc = "新增用户")
    @GetMapping("/add")
    public String add(){
        return "新增用户";
    }
    @OperLog(operModule = "系统管理",operType = "修改",operDesc = "修改用户")
    @GetMapping("/modify")
    public String modify(){
        return "修改用户";
    }
    @OperLog(operModule = "系统管理",operType = "查询",operDesc = "查询用户")
    @GetMapping("/list")
    public String list(){
        return "查询用户";
    }
    @OperLog(operModule = "系统管理",operType = "删除",operDesc = "删除用户")
    @GetMapping("/remove")
    public String remove(){
        return "删除用户";
    }
}
