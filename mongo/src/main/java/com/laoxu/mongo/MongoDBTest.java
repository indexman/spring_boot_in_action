package com.laoxu.mongo;


import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

/**
 * 测试MongoDB
 *
 * @author xusucheng
 * @create 2019-01-10
 **/
public class MongoDBTest {
    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient("localhost",27017) ;
        MongoDatabase md = mongoClient.getDatabase("db");
        System.out.println("Connect successfully");

    }
}
