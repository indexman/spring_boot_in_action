package com.laoxu.springboot.mapper;

import com.laoxu.springboot.entity.Department;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

/**
 * 部门DAO
 * @author xusucheng
 * @create 2019-01-18
 **/
@Repository
@Mapper
public interface DepartmentMapper {
    @Select("select id, department_name as departmentName from department where id=#{id}")
    Department getDeptById(Integer id);

    @Delete("delete from department where id=#{id}")
    int deleteDeptById(Integer id);

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into department(department_name) values(#{departmentName})")
    int insertDept(Department department);

    @Update("update department set department_name=#{departmentName} where id=#{id}")
    int updateDept(Department department);
}
