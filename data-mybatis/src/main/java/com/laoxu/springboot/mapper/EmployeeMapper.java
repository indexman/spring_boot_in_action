package com.laoxu.springboot.mapper;

import com.laoxu.springboot.entity.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author xusucheng
 * @create 2019-01-18
 **/
@Repository
@Mapper
public interface EmployeeMapper {
    Employee getEmpById(Integer id);

    void insertEmp(Employee employee);
}
