package com.laoxu.springboot.controller;

import com.laoxu.springboot.entity.Department;
import com.laoxu.springboot.entity.Employee;
import com.laoxu.springboot.mapper.DepartmentMapper;
import com.laoxu.springboot.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 部门控制器
 *
 * @author xusucheng
 * @create 2019-01-18
 **/
@RestController
public class DepartmentController {
    @Autowired
    DepartmentMapper departmentMapper;

    @Autowired
    EmployeeMapper employeeMapper;

    @GetMapping("/dept/{id}")
    public Department getDept(@PathVariable("id") Integer id){
        return departmentMapper.getDeptById(id);
    }

    @GetMapping("/dept")
    public String addDept(Department department){
        int id = departmentMapper.insertDept(department);

        return "添加成功部门："+id;
    }

    @GetMapping("/emp/{id}")
    public Employee getEmp(@PathVariable("id") Integer id){
        return employeeMapper.getEmpById(id);
    }



}
