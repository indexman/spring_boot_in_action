package com.laoxu.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 测试
 * @Author laoxu
 * @Date 2020/6/13 23:14
 **/
@Slf4j
@RestController
public class HelloController {
    @GetMapping("/greetings/{username}")
    public String getGreetings(@PathVariable("username") String userName) {
        log.info("服务器获取到页面参数：{}",userName);
        return "Hello " + userName + ", Good day...!!!";
    }
}
