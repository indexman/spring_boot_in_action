package com.laoxu.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * @author xusucheng
 * @create 2019-01-10
 **/
@Controller
public class HelloController {
    @RequestMapping("/hello")
    @ResponseBody
    public String sayHello() {
        return "Hello Spring Boot!";
    }

    /* @RequestMapping({"/","/login.html"})
     public String index(){
         return "index";
     }*/
    @RequestMapping("/changeLanguage")
    public String changeLanguage(HttpServletRequest request, String lang) {
        System.out.println(lang);
        if ("zh".equals(lang)) {
            //代码中即可通过以下方法进行语言设置
            request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale("zh", "CN"));
        }
        else if("en".equals(lang)) {
            //代码中即可通过以下方法进行语言设置
            request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale("en","US"));
        }
        return "redirect:/index.html";
    }
}
