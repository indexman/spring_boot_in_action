package com.laoxu.amqp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xusucheng
 * @create 2019-01-29
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class AmqpAdminTest {
    @Autowired
    AmqpAdmin amqpAdmin;

    /**
     * 创建Exchange, Queue
     */
    @Test
    public void testCreateExchange(){
        amqpAdmin.declareExchange(new DirectExchange("exchange.laoxu.direct"));
        amqpAdmin.declareQueue(new Queue("laoxu.queue",true));
        amqpAdmin.declareBinding(new Binding("laoxu.queue", Binding.DestinationType.QUEUE,"exchange.laoxu.direct","laoxu.#",null));
    }

    @Test
    public void testDeleteExchange(){
        amqpAdmin.deleteQueue("laoxu.queue");
        amqpAdmin.deleteExchange("exchange.laoxu.direct");
    }
}
