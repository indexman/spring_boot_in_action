package com.laoxu.amqp;

import com.laoxu.amqp.bean.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AmqpApplicationTests {
    @Autowired
    RabbitTemplate rabbitTemplate;


    /**
     * 发送消息
     */
    @Test
    public void contextLoads() {
        Map<String,Object> data = new HashMap<>();
        data.put("msg","来自spring boot第一条消息");
        data.put("data", Arrays.asList("helloworld",123,true));
        rabbitTemplate.convertAndSend("exchage.direct","atguigu",data);
    }

    /**
     * 发送bean
     */
    @Test
    public void testSendBean() {
        Book book = new Book("《西游记》","吴承恩");
        rabbitTemplate.convertAndSend("exchage.direct","atguigu",book);
    }

    /**
     * 接收消息
     */
    @Test
    public void testReciveMsg(){
        Object o = rabbitTemplate.receiveAndConvert("atguigu");
        System.out.println(o.getClass());
        System.out.println(o);
    }

    /**
     * 广播消息
     */
    @Test
    public void testFanOut(){
        rabbitTemplate.convertAndSend("exchage.fanout","atguigu.news","各单位注意，明天开始放假！");
    }

}

