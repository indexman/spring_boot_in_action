package com.laoxu.amqp.service;

import com.laoxu.amqp.bean.Book;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * @author xusucheng
 * @create 2019-01-29
 **/
@Service
public class BookService {
    @RabbitListener(queues = "atguigu")
    public void receive(Book book){
        System.out.println("收到消息："+book);
    }
}
