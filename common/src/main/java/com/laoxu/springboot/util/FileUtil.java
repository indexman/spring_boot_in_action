package com.laoxu.springboot.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * 文件工具类
 *
 * @author xusucheng
 * @create 2019-02-16
 **/
public class FileUtil {
    public static String read(String filePath){
        StringBuffer sb = new StringBuffer();
        try {
            FileReader fileReader = new FileReader(filePath);
            //一次读取10行
            BufferedReader bf = new BufferedReader(fileReader);
            String line = "";
            int curLineNum=0;
            while(curLineNum<10 && (line=bf.readLine())!=null){
                sb.append(line+"\n");
                curLineNum++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(System.getProperty("user.home"));
        //System.out.println(read("F:\\BaiduNetdiskDownload\\看见.txt"));
    }
}
