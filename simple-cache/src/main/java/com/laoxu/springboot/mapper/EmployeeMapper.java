package com.laoxu.springboot.mapper;

import com.laoxu.springboot.bean.Employee;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface EmployeeMapper {
    @Select("SELECT * FROM employee WHERE id=#{id}")
    Employee getEmpById(Integer id);

    @Update("update employee set last_name=#{lastName}, email=#{email},gender=#{gender},d_id=#{dId} where id=#{id}")
    void updateEmp(Employee employee);

    @Delete("delete from employee where id=#{id}")
    void deleteEmpById(Integer id);

    @Insert("insert into employee(last_name,email,gender,d_id)  values(#{lastName}, #{email}, #{gender}, #{dId})")
    void insertEmp(Employee employee);

    @Select("SELECT * FROM employee WHERE last_name=#{lastName}")
    Employee getEmpByLastName(String lastName);

}
