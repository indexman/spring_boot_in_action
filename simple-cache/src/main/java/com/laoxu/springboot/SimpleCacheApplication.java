package com.laoxu.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@MapperScan("com.laoxu.springboot.mapper")
@EnableCaching//启用缓存
@SpringBootApplication
public class SimpleCacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleCacheApplication.class, args);
    }

}

