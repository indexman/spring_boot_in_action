package com.laoxu.springboot;

import com.laoxu.springboot.bean.Employee;
import com.laoxu.springboot.mapper.EmployeeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleCacheApplicationTests {
    @Autowired
    EmployeeMapper employeeMapper;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    //注入自定义RedisTemplate
    @Autowired
//    RedisTemplate<Object,Employee> myRedisTemplate;

    @Test
    public void test01(){
        stringRedisTemplate.opsForValue().append("msg","Hello World!");
        String msg = stringRedisTemplate.opsForValue().get("msg");
        System.out.println(msg);
    }

    //测试保存Employee对象
    @Test
    public void testSaveEmp(){
        Employee emp = employeeMapper.getEmpById(1);
        //默认保存对象使用的jdk序列化
        redisTemplate.opsForValue().set("emp01", emp);
        //使用自定义JSON序列化器
        //myRedisTemplate.opsForValue().set("emp01", emp);
    }

    @Test
    public void contextLoads() {
        Employee emp = employeeMapper.getEmpById(1);
        System.out.println(emp);
    }

}

